# Arkanoid

Developed with Unreal Engine 4

## Deskription

Created a simple Arkanoid game to try to make a project on Blueprints

![Solution]( Images/Screenshot.png "Screenshot")

Each time the project is started, the blocks are randomly filled. There are different types of blocks. For example, there are blocks that do not collapse, but there are blocks that need to be hit several times. There are also bonus elements that can increase the carriage and there are those that slow the ball down or duplicate the ball.